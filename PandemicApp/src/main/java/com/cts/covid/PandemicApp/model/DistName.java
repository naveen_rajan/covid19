package com.cts.covid.PandemicApp.model;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistName {
	int reportedcases;
	int activecases;
	int deceasedcases;
	int recoveredcases;
	String name;
}
