package com.cts.covid.PandemicApp.model;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class States {
	
	private String stateName;
	List<DistName> distName;
}
